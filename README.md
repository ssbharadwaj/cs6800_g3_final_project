# ALS Knowledge Graph Project

## Overview

This project aims to create a comprehensive Knowledge Graph (KG) for Amyotrophic Lateral Sclerosis (ALS) by integrating biomedical data from various sources. The KG will serve as a valuable resource for researchers, clinicians, and anyone interested in understanding ALS better.

## Project Structure

The project is organized into several key components:

1. **Data Collection and Preprocessing**: In this phase, we gather ALS-related research articles from PubMed using the Entrez API. The collected data undergoes preprocessing to ensure it's organized and ready for analysis.

2. **Entity Extraction**: We extract key entities from the collected texts, such as symptoms, treatments, genes, and diseases. Initially, we explored the use of BioBERT, a complex AI model, for entity extraction. However, we later adopted a rule-based approach for better accuracy.

3. **Relation Extraction**: The next step involves extracting relationships between entities, uncovering how different elements within the ALS domain are connected. This process is crucial for constructing an interconnected KG.

4. **Knowledge Graph Construction**: We use NetworkX, a Python library, to construct the ALS Knowledge Graph. Entities such as genes, drugs, and diseases are represented as nodes, while relationships are depicted as edges with weights to indicate their strength.

5. **Knowledge Graph Visualization**: We utilize Matplotlib to create clear visual representations of the KG. The spring layout algorithm helps position nodes, and edges are labeled to show relationship types.

6. **Report and Documentation**: The project includes comprehensive documentation and a research report that explains the methodology, challenges, and findings.


## Future Enhancements

Our research is ongoing, and we plan to continuously improve and expand the ALS Knowledge Graph. Future enhancements may include:

- Incorporating more diverse data sources.
- Enhancing entity extraction methods for greater accuracy.
- Exploring advanced techniques for relation extraction.
- Developing user-friendly interfaces for KG exploration.

## Getting Started

To run this project on your local machine, follow these steps:

1. [Clone the repository](#) to your local environment.
2. Install the required Python libraries.
3. Added commented lines along the code for better understanding of how the code works.
